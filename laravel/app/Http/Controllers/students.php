<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\student;
use Illuminate\Support\Facades\Input;
use Hash;
use Lang;
use Session;

class students extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
         $students = student::all();
        return view('students.index')->with('students',$students);
        

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('students.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //getting up data and store
      $this->validate($request,[
         'name'=>'required',
         'password'=>'required',
         'email'=>'required|email',
         'rollno'=>'required|numeric',
         'phone'=>'required|numeric|digits:10',
         'address'=>'required',
         'gender'=>'required',
         'img'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:4800',
         'city'=>'required'             
            ]);
        $img = $request->file('img');
        
        $tmpname = 'student_'.$request->input('rollno');

        $name = $tmpname.".".$img->getClientOriginalExtension();
        $path = '../uploads/'.$name;

        $r = $img->move('uploads',$name);

        
        $hobbya = Input::get('Hobby');

        $hobby="";
        $counter=0;

        foreach ($hobbya as $key=>$value) 
        {
            if (isset($value)) 
            {
                if ($counter==1) 
                {
                    $hobby.=",".$value; 
                }
                else
                {
                    $counter++;
                    $hobby.=" ".$value;    
                }
            }
                
        }
        $student = new student();
        $student->id=0;
        $student->img = $path;
        $student->name=$request->input('name');
        $student->password = ($request->input('password'));
        $student->email=$request->input('email');
        $student->rollno=$request->input('rollno'); 
        $student->phone=$request->input('phone');
        $student->address=$request->input('address');
        $student->gender=$request->get('gender');
        $student->hobby=$hobby;
        $student->city=$request->input('city');
        $student->updated_at=time();
        if($student->save())
        {
            return view('students.index')->with('students',student::all());
        }
        else
        {
            return view('students.index')->with('message','Data Not Inserted');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $student = student::find($id);
        $student->delete();
        return view('students.index')->with('students',student::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $student = student::find($id);
        return view('students.edit',['students'=>$student]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        Getting user data
        */
         $this->validate($request,[
         'name'=>'required',
         'email'=>'required|email',
         'rollno'=>'required|numeric',
         'phone'=>'required|max:10|min:10',
         'address'=>'required',
         'gender'=>'required',
         'city'=>'required'             
            ]);

         $student = new Student();
         $student = Student::find($id);
         $hobbya = Input::get('Hobby');
         $hobby="";
         $counter =0;       
        foreach ($hobbya as $key => $value) 
        {
                if ($counter==1) 
                {
                    $hobby.=",".$value; 
                       
                }
                else
                {
                     $counter++;
                    $hobby.=" ".$value;    
                }
                
        }
        $img = $request->file('img');
        if (isset($img)) 
        {
             $name = $tmpname.".".$img->getClientOriginalExtension();
             $tmpname = 'student_'.$request->input('rollno');
             $path = '../uploads/'.$name;
             $r = $img->move('uploads',$name);
             $student->img = $path;

        }
        $student->name=$request->input('name');
        $student->email=$request->input('email');
        $student->rollno=$request->input('rollno'); 
        $student->phone=$request->input('phone');
        $student->address=$request->input('address');
        $student->gender=$request->get('gender');
        $student->hobby=$hobby;
        $student->city=$request->input('city');
        $student->updated_at=time();
        $student->save();
    
      return view('students.index')->with('students',student::all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request)
    {
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        $student = Student::where('email',$request['email'])->where
        ('password',$request['password'])->first();


        if(isset($student))
        {
            Session::put('userid', $student->id);
            return view('students.profile')->with('students',$student);
        }      
        else
        {
           return redirect()->back()->withErrors([
                         'email' => Lang::get('auth.failed'),
                                                 ]);
        }
    }
    public function destroy($id)
    {
      
    }
}
