<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use URL;
use DB;
use User;
use App\Quotation;

class view extends Controller
{
    	
	public function index()
	{
		$student = DB::select("SELECT * FROM student ");
		return view('display',['student'=>$student]);	
	}

	public function delete($id)
	{
		$rollno = intval($id);
	
		$result = DB::delete('DELETE FROM student WHERE rollno=?',[$id]);
		if (isset($result)) 
		{
			echo "<script>alert('Data deleted successfully')</script>";	
			return redirect('/showstudent');
		}
		else
		{
			echo("Somthing went wrong");
		}
	}
	public function insert(Request $request)
    {
    	//getting user submited data...
    	$name = $request->input('name');
    	$email = $request->input('email');
    	$rollno =$request->input('rollno');	
    	$phone = $request->input('phone');
    	$address =$request->input('address');
        $gender = $request->get('gender');
        $hobbya = Input::get('hobby');
        $hobby="";
        foreach ($hobbya as $key => $value) 
        {
            if (isset($value)) {
            $hobby.=" ,".$value;    
            }
                
        }
        $city = $request->input('city');
        $password = $request->input('pass');

         $user = new User;
         $user->name = $request->get('name');
         $user->save();

         


    	//Inserting Record to database 
         $sql = "INSERT INTO student(name,email,password,rollno,phone,address,gender,Hobby,City) VALUES(?,?,?,?,?,?,?,?,?)";
    	DB::insert($sql,[$name,$email,md5($password),$rollno,$phone,$address,$gender,$hobby,$city]);
    	echo '<script>alert("data inserted successfully") </script>';
    	echo "Insert More Record ? <a href='/register'> Click here..</a><br>";
    	echo "Show Records..<a href='/showstudent'> Click here..</a><br>";

    }
    public function update($id)
    {
    	$student = DB::select("SELECT * FROM student WHERE rollno=?",[$id]);
    	return view('edit',['student'=>$student]);
    }

    public function edit(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $rollno =$request->input('rollno'); 
        $phone = $request->input('phone');
        $address =$request->input('address');
        $gender = $request->get('gender');
        $hobbya = Input::get('hobby');
        $hobby="";
        foreach ($hobbya as $key => $value) 
        {
            if (isset($value)) 
            {
                 $hobby.="".$value;    
            }            
        }
        $city = $request->input('city');

        //Inserting Record to database 
         $sql = "UPDATE student SET name=?,email=?,phone=?,address=?,gender=?,Hobby=?,City=? WHERE rollno";
        DB::insert($sql,[$name,$email,$phone,$address,$gender,$hobby,$city,$rollno]);
        echo '<script>alert("data updated successfully") </script>';
        echo "Show Records .. <a href='/showstudent'> Click here..</a><br>";
    }

}
