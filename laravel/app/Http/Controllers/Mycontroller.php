<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use DB;

class Mycontroller extends BaseController
{
	  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	   public function cal(Request $request)
       {
    	 $no1 = $request->input('no1');
    	 $no2 = $request->input('no2');
    	 $op = $request->input('opration');
 
 			switch ($op)
 			 {
 				case '+':
 					$ans = $no1 + $no2;
 					echo 'Answer is : '.$ans;
 					break;

 				case '-':
 					$ans = $no1 - $no2;
 					echo 'Answer is :'.$ans;
 					break;

 				case '*':
 					$ans = $no1 * $no2;
 					echo 'Answer is :'. $ans;
 					break;

 				case '/':
 					if($no2==0)
 					{
 						echo 'not Divided by zero';
 						break;
 					}
 					else
 					{
 						$ans = $no1 / $no2;
 						echo 'Answer is :'.$ans;
 						break;
 					}
 					break;

 				case 'pow':
 					{
 						$ans = pow($no1,$no2);
 						echo 'Answer is : '.$ans;
 						break;
 					}
 				default:
 					echo 'Somthing Went Wrong';
 					break;
 		}

    }
   
}