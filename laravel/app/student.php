<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class student extends Model implements \Illuminate\Contracts\Auth\Authenticatable
{
 	protected $table = 'students' ;
	public $primaryKey = 'id';
	public $timestamps = 'true';
	 protected $fillable = ['name','email','password','phone','rollno','address','gender','hobby','city','created_at','updated_at'];

	public function getAuthIdentifierName()
	{

	}
	public function getAuthIdentifier(){}
	public function getAuthPassword(){}
	public function getRememberToken(){}
	public function setRememberToken($value){}
	public function getRememberTokenName(){}
}
