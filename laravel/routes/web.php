<?php

use App\student;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('students.home');
});

Route::get('form',function(){
	return view('form');
});

Route::get('calculator',function(){
	return view('calc');
});

Route::get('register',function(){
	return view('registration');
});

Route::post('submit','Controller@show');

Route::post('calculate','Mycontroller@cal');

/*Route::post('insert','view@insert');

Route::get('showstudent','view@index');

Route::get('delete/{id}','view@delete');

Route::get('update/{id}','view@update');

Route::post('update','view@edit');

Route::get('login',function(){
	return view('login');
});*/
	Route::get('/students',function(){
	return  view('students.login');
})->middleware('auth');

Route::post('/students/login','students@login');

Route::get('/students/login', function(){
	return view('students.login');
})->middleware('auth');

Route::post('validate','login@validateuser');
Route::get('validate',function(){ return view('login');});

Route::get('password',function(){
	return view('recovery');
});


Route::get('/students/profile',function(){
	return view('students.login');
});//->middleware('profile');

Route::get('/students/register',function(){

	return view('students.index')->with('students',student::all());
})->middleware('auth');

Route::resource('students','students')->middleware('auth');

Auth::routes();


Route::get('/students',function(){
	return  view('students.login');
})->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home');
