@extends('layouts.app')
@section('content')
<center>
	<h1>Edit Profile</h1><br>
	 @if (count($errors) > 0)
         <div class = "alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
      @endif
	<form action="/students/{{$students->id}}" method="post" class="form-group" enctype="multipart/form-data">
	 {{ csrf_field() }}
	     <input type="hidden" name="_method" value="PUT">
		<table>
			<tr>
				<th>Name :</th>
				<td><input type="text" value="{{$students->name}}" name="name" class="form form-control"></td>
			</tr>
			<tr>
				<th>Image</th>
				<td><input type="file" name="img" class="btn btn-info form-control"></td>
			</tr>
			<tr>
				<th>Email :</th>
				<td><input type="email" value="{{$students->email}}" name="email" class="form form-control"></td>
			</tr>
			<tr>
				<th>Roll No :</th>
				<td><input type="number" value="{{$students->rollno}}" name="rollno" class="form form-control"></td>
			</tr>
			<tr>
				<th>Phone :</th>
				<td><input type="number" value="{{$students->phone}}" name="phone" class="form form-control"></td>
			</tr>
			<tr>
				<th>Address :</th>
				<td><textarea name="address" cols="30" rows="5" class="form form-control">{{$students->address}}</textarea></td>
			</tr>
			<tr>
				<th>Gender :</th>
				@if(strcasecmp($students->gender,"male")==0)
					  	    <td>Male : <input type="radio" name="gender" value="Male" checked >
                                Female: <input type="radio" name="gender" value="Female">
                            </td>
					   	@else
					   	<td>Male : <input type="radio" name="gender" value="Male" >
                                Female: <input type="radio" name="gender" value="Female" checked>
                            </td>
					   	@endif
			</tr>
			<tr>
				<?php 
					$arr = explode(",",$students->hobby);
				
				?>
				<th>Hobby :</th>
				<td>Coding : <input type="checkbox" name="Hobby[]" value="coding" <?php
				 if(in_array("coding", $arr)) 
				 {
				   echo "checked";
				 }
				?>>
		
				<br>
					Reading : <input type="checkbox" name="Hobby[]" value="Reading" <?php 
					if(in_array("Reading",$arr))
					{
						echo "checked";
					}
					 ?>>
				<br>
					Writing : <input type="checkbox" name="Hobby[]" value="Writing" <?php 
					if(in_array("Writing", $arr))
					{
						echo "checked";
					}
					 ?>>
				<br>
					Music : <input type="checkbox" name="Hobby[]" value="Music" <?php 
					if(in_array("Music", $arr))
					{
						echo "checked";
					}
					 ?>>
				<br>
				</td>
			</tr>
			<tr>
				<th>City :</th>
				<td><select name="city" class="form form-control">
					<option value="Ahmedabad">Ahmedabad</option>
					<option value="Surat">Surat</option>
					<option value="Baroda">Baroda</option>
					<option value="Pune">Pune</option>
				</select></td>
			</tr>
			<tr>
				<th></th>
				<td><input type="submit" name="submit" class="btn btn-primary" value="Submit"></td>
			</tr>
		</table>
	</form>
</center>

@endsection