@extends('layouts.app')

@section('content')
<div class="container">
	@if(count($students)>0)
	<center><h1>Students Records..</h1></center>
		<table class="table table-responsive table-striped">
			<tr class=" bg-primary">
				<th>Name</th>
				<th>Email</th>
				<th>Rollno</th>
				<th>Phone</th>
				<th>Address</th>
				<th>Gender</th>
				<th>hobby</th>
				<th>City</th>
				<th>Option</th>
			</tr>
		@foreach($students as $student)
			<tr class="bg-default">
				<td>{{$student->name}}</td>
				<td>{{$student->email}}</td>
				<td>{{$student->rollno}}</td>
				<td>{{$student->phone}}</td>
				<td>{{$student->address}}</td>
				<td>{{$student->gender}}</td>
				<td>{{$student->hobby}}</td>
				<td>{{$student->city}}</td>
				<td><a href="/students/{{$student->id}}/edit">Edit</a> | <a href="/students/{{$student->id}}">Delete</a></td>
			</tr>
		@endforeach
		</table>
		Insert Records ? &nbsp;<a href="/students/create" class="btn btn-primary">Click here</a>
		@else
			<script type="text/javascript">
				alert('Nothing To show');
			</script>
			Insert Records ? &nbsp;<a href="/students/create" class="btn btn-primary">Click here</a>
		@endif
		
</div>
@endsection