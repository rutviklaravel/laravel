@extends('layouts.app')

@section('content')
<center>
	<h1>Register Student</h1><br>
	 @if (count($errors) > 0)
         <div class = "alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
      @endif
	<form action="{{URL::to('/students')}}" method="post" class="form-group" enctype="multipart/form-data">
	 {{ csrf_field() }}
		<table>
			<tr>
				<th>Name :</th>
				<td><input type="text" name="name" class="form form-control"></td>
			</tr>
			<tr>
				<th>Image</th>
				<td><input type="file" name="img" class="btn btn-info form-control"></td>
			</tr>
			<tr>
				<th>Email :</th>
				<td><input type="text" name="email" class="form form-control"></td>
			</tr>
			<tr>
				<th>Password :</th>
				<td><input type="password" name="password" class="form form-control"></td>
			</tr>
			<tr>
				<th>Roll No :</th>
				<td><input type="text" name="rollno" class="form form-control"></td>
			</tr>
			<tr>
				<th>Phone :</th>
				<td><input type="text" name="phone" class="form form-control"></td>
			</tr>
			<tr>
				<th>Address :</th>
				<td><textarea name="address" cols="30" rows="5" class="form form-control"></textarea></td>
			</tr>
			<tr>
				<th>Gender :</th>
				<td>Male <input type="radio" name="gender" value="Male">
					Female <input type="radio" name="gender" value="Female">
				</td>
			</tr>
			<tr>
				<th>Hobby :</th>
				<td>Coding : <input type="checkbox" name="Hobby[]" value="coding" ><br>
					Reading : <input type="checkbox" name="Hobby[]" value="Reading"><br>
					Writing : <input type="checkbox" name="Hobby[]" value="Writing"><br>
					Music : <input type="checkbox" name="Hobby[]" value="Music"><br>
				</td>
			</tr>
			<tr>
				<th>City :</th>
				<td><select name="city" class="form form-control">
					<option value="Ahmedabad">Ahmedabad</option>
					<option value="Surat">Surat</option>
					<option value="Baroda">Baroda</option>
					<option value="Pune">Pune</option>
				</select></td>
			</tr>
			<tr>
				<th></th>
				<td><input type="submit" name="submit" class="btn btn-primary" value="Submit"></td>
			</tr>
		</table>
	</form>
</center>
@endsection