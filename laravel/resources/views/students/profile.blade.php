@extends('layouts.app')
@section('content')
@if(Session::has('userid'))
<style>
    .title{
        font-family: sans-serif;
    }   
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card"> 
                <div class="card-header bg-info"><h1 class="title text-center">Student Profile </h1></div>
                <div class="card-body">
                     <div class="row justify-content-center">
                        <div class="col-auto">
                   
                        <img src="{{$students->img}}" alt="{{$students->name}}" class="rounded" height="200px" width="200px"><br>
                            <table class="table table-responsive">
                                <tr>
                                    <th>Name :</th>
                                    <td>{{$students->name}}</td>             
                                </tr>
                                <tr>
                                    <th>Email :</th>
                                    <td>{{$students->email}}</td>
                                </tr>
                                <tr>
                                    <th>Rollno :</th>
                                    <td>{{$students->rollno}}</td>
                                </tr>
                                <tr>
                                    <th>Phone No:</th>
                                    <td>{{$students->phone}}</td>
                                </tr>
                                <tr>
                                    <th>Address :</th>
                                    <td>{{$students->address}}</td>
                                </tr>
                                <tr>
                                    <th>Hobby :</th>
                                    <td>{{$students->hobby}}</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td><a href="/students/{{$students->id}}/edit" class="btn btn-primary">Update Profile . </a></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@else
{
    
   <script>
   window.location = "/students/login";
   </script>
}
@endif
@endsection
