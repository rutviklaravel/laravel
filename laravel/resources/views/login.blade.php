<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
</head>
<body>
	<form action="{{URL::to('validate')}}" method="post" >
		{{ csrf_field() }}
		<table>
			<tr>
				<th><h1>Login here</h1></th>
				<td></td>
			</tr>
			<tr>
				<th>Email :</th>
				<td><input type="email" name="email"  placeholder="Enter Email" required></td>
			</tr>
			<tr>
				<th>Password :</th>
				<td><input type="password" name="pass" placeholder="Enter Your Password" required></td>
			</tr>
			<tr>
				<th></th>
				<td><button type="submit" name="submit">Login</button></td>
			</tr>
		</table>
	</form>
</body>
</html>