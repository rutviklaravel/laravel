<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Profile of user</title>
</head>
<body>
	@if(Session::get('user'))
	@foreach($user as $data)
	 <table>
		<tr>
			<th></th>
			<th><h1>{{ $data->name }} Profile</h1></th>
		</tr>
		<tr>
			<th>Name :</th>
			<td>{{ $data->name}}</td>
		</tr>
		<tr>
			<th>Email :</th>
			<td>{{$data->email}}</td>
		</tr>
		<tr>
			<th>Rollno :</th>
			<td>{{$data->rollno}}</td>
		</tr>
		<tr>
			<th>Phone: </th>
			<td>{{$data->phone}}</td>
		</tr>
		<tr>
			<th>Address :</th>
			<td>{{$data->address}}</td>
		</tr>
		<tr>
			<th>Gender :</th>
			<td>{{$data->gender}}</td>
		</tr>
		<tr>
			<th>Hobbies :</th>
			<td>{{$data->Hobby}}</td>
		</tr>
		<tr>
			<th>City :</th>
			<td>{{$data->City}}</td>
		</tr>
	</table>
		<p>Change Password : <a href="/password">Click Here</a></p>
	@endforeach
	@else
		<h1>Login First..</h1><a href="/login">Click here</a>
	@endif
</body>
</html>