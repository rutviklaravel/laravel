<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>  <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <div class="content">
            	<h1>Register Yourself Here..</h1>
				<form action="{{ URL::to('insert') }}" method="post" accept-charset="utf-8">
					{{ csrf_field() }}
                    <table>
                        <tr>
                            <th>Name :</th>
                            <td><input type="text" minlength="4" name="name" placeholder="Enter name" required></td>
                        </tr>
                        <tr>
                            <th>Email :</th>
                            <td><input type="email" name="email" required placeholder="Enter email"><br></td>
                        </tr>
                        <tr>
                            <th>Roll No :</th>
                            <td> <input type="number" name="rollno" placeholder="Enter Roll No" required></td>
                        </tr>
                        <tr>
                            <th>Password :</th>
                            <td><input type="password" name="pass" required></td>
                        </tr>
                        <tr>
                            <th>phone :</th>
                            <td><input type="number" minlength="10" name="phone" placeholder="Enter Mobile No" required></td>
                        </tr>
                        <tr>
                            <th>Address :</th>
                            <td><textarea name="address" rows="5" required  cols="25" placeholder="Enter Address here"></textarea></td>
                        </tr>
					   <tr>
                            <th>Gender : </th>               
                            <td>Male : <input type="radio" name="gender" value="Male">
                                Female: <input type="radio" name="gender" value="Female">
                            </td>
                       </tr>
                        <tr>
                            <th>Hobby :</th>
                            <td>Dance<input type="checkbox" name="hobby[]" value="Dancing"><br>
                           Write<input type="checkbox" name="hobby[]" value="Writing"><br>
                           Code<input type="checkbox" name="hobby[]" value="Coding"><br>
                           Music<input type="checkbox" name="hobby[]" value="Music"><br>
                           Travel<input type="checkbox" name="hobby[]" value="Traveling"></td>
                        </tr>
                        <tr>
                            <th>City :</th>
                            <td><select name="city" required>
                            <option value="Ahmedabad" value="Ahmedabad">Ahmedabad</option>
                            <option value="Surat" value="Surat">Surat</option>
                            <option value="Valsad" value="Valsad">Valsad</option>
                            <option value="Baroda" value="Baroda">Baroda</option>
                           </select></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><button type="submit">Submit</button></td> 
                        </tr>
				</table>
                </form>
			</div>
        </div>
    </body>
</html>