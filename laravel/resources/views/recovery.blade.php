<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Password Recovery</title>
</head>
<body>
	@if(Session::get('user'))
	<!-- Display form of setting new password -->
	<form action="{{URL::to('recovery')}}">
	 <h3>New Password :</h3><input type="password" name="password"  placeholder="Enter Password"><br><br>
	 <button type="submit">Submit</button>
	</form>
	@else
	<!-- Redirect to another page  -->
		{{URL::to('login')}}
	@endif
</body>
</html>