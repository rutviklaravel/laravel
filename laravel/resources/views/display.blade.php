<!DOCTYPE html>
<html>
<head>
	<title>Registered Student Details</title>
	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
</head>
<body>
	<table border="1">
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Rollno</th>
			<th>Phone</th>
			<th>Address</th>
			<th>Gender</th>
			<th>Hobby</th>
			<th>City</th>
			<th>Option </th>
		</tr>
		@foreach($student as $data)
		<tr>
			<td> {{ $data->name   }} </td>
			<td> {{ $data->email  }} </td>
			<td> {{ $data->rollno }} </td>
			<td> {{ $data->phone  }} </td>
			<td> {{ $data->address}} </td>
			<td> {{ $data->gender }} </td>
			<td> {{ $data->Hobby  }} </td>
			<td> {{ $data->City   }} </td>
			<td><a href="/delete/{{$data->rollno}}">Delete</a> | <a href="/update/{{$data->rollno}}">Update</a></td>
		</tr>
		@endforeach
	</table>
	 Insert More Records<a href='/register'> Click here..</a>
	</body>
</html>