<?php
include('testconn.php');
include('header.php');


if(empty($_REQUEST['id'])){
	echo '<div class="container"><div class="alert alert-danger">No Data Found </div> </div>';
    exit;
}


$sql = "SELECT * FROM doctor_request where id=".$_REQUEST['id'];
$result = mysql_query($sql);

if (mysql_num_rows($result) > 0) {

	// output data of each row
$row = mysql_fetch_array($result);


if(isset($_POST['submit'])){

	echo '<div class="container"><div class="alert alert-success">Your data sent successfully , You Will notify by email when your profile updated  </div> </div>';

	$to  = 'prasanth.rajan@medcarehospital.com' ;
	$subject = 'New Doctor profile has been submitted - '.$row['name'];
	$message = '
	<html>
	<body>
	  <p>Dear HR Team, </p>
	  <p>We want to inform you that we received a new doctor profile on the system for <b>('.$row['name'].')</b> <i>('.$row['bran'].')</i>. You can now go to the <a href="http://www.medcare.ae/doctors">system</a> to take the action for it.</p>
	  <p>Best Regards,<br />Medcare System</p>
	 
	<h2></h2>
	</body>
	</html>
	';

	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	// Additional headers
	$headers .= 'From: Marketing Team <emarketing@medcarehospital.com>' . "\r\n";
	$headers .= 'Cc: marketing@medcarehospital.com' . "\r\n";
	// Mail it
	mail('yogesh.kh@latitudetechnolabs.com', $subject, $message, $headers);
	exit;

}

?>

<style type="text/css">
	.panel-heading{
background:#880656!important;
color:white!important;
}
.panel{
border-color:#880656!important;
}
.purber{
background:#880656!important;
color:white!important;
}
</style>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div id="home">
      <div class="container">
	
	 
	 
	 
	<form class="form form-horizontal sdff" method="post">			
				
		<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">Personal Informations</h3>
  </div>
  <div class="panel-body">
  
<div class="form-group">
								<label class="col-sm-3 control-label">Purpose  	</label>
								<div class="col-sm-9">
<select name="purpose" class="form-control" disabled>
<?php
    $is_selected = 'selected';
    $not_selected = '';
    if($row['purpose'] == 'New Profile'){
        $is_selected = 'selected';
    }else{
        $not_selected = 'selected';
        $is_selected = '';
    }
?>
<option <?=$is_selected?>>New Profile</option>
<option <?=$not_selected?>>Edit Profile on website</option>

</select>
								</div>
							</div>
					
							<div class="form-group">
								<label class="col-sm-3 control-label">Name  	</label>
								<div class="col-sm-9">
									<input type="text" name="name" required class="form-control" value="<?=isset($row['name'])?$row['name']:'' ?>" readonly>
								</div>
							</div>

							<div class="form-group">
								<label for="inputEmail3" class="col-sm-3 control-label">Email </label>
								<div class="col-sm-9">
									<input type="email" name="email"  class="form-control" id="inputEmail3" value="<?=isset($row['email'])?$row['email']:'' ?>" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Mobile </label>
								<div class="col-sm-9">
									<input type="text" name="mobile"  class="form-control" value="<?=isset($row['mobile'])?$row['mobile']:'' ?>" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Languages Spoken </label>
								<div class="col-sm-9">
									<input type="text" name="lang"  class="form-control" value="<?=isset($row['lang'])?$row['lang']:'' ?>" readonly>
								</div>
							</div>
							
						
							
												
						

  </div>
</div>




	<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Career Informations</h3>
  </div>
  <div class="panel-body">
  

							<div class="form-group">
								<label class="col-sm-3 control-label">Nature of Appointment </label>
								<div class="col-sm-9">
									<?php
									    $is_selected = 'selected';
									    $not_selected = '';
									    if($row['appo'] == 'Full Time'){
									        $is_selected = 'selected';
									    }else{
									        $not_selected = 'selected';
									        $is_selected = '';
									    }
									?>
									<select name="appo" class="form-control"  disabled="">
                                        <option value="Full Time" <?=$is_selected?>>Full Time</option>
                                        <option value="Part Time" <?=$not_selected?>>Part Time</option>
                                    </select>


								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label"> Branches  </label>
								<div class="col-sm-9">
									<select name="bran[]" multiple="" class="form-control"  style="height:200px;" disabled="">
                                        <option value="Medcare Hospital" readonly>Medcare Hospital</option>
                                        <option value="Medcare Medical Centre Mirdif" readonly>Medcare Medical Centre Mirdif</option>
                                        <option value="Medcare Medical Centre Mirdif City Centre" readonly>Medcare Medical Centre Mirdif City Centre</option>
                                        <option value="Medcare Medical Centre Jumeirah" readonly>Medcare Medical Centre Jumeirah</option>
                                        <option value="Medcare Orthopaedics &amp; Spine Hospital" readonly>Medcare Orthopaedics &amp; Spine Hospital</option>
                                        <option value="Medcare Medical Centre JBR" readonly>Medcare Medical Centre JBR</option>
                                        <option value="Medcare Physio &amp; Rehab Centre" readonly>Medcare Physio &amp; Rehab Centre</option>
                                        <option value="Medcare Paediatric Speciality Clinic" readonly>Medcare Paediatric Speciality Clinic</option>
                                        <option value="Medcare Speciality Centre"readonly>Medcare Speciality Centre</option>
                                        <option value="Medcare Medical Centre Discovery Gardens"readonly>Medcare Medical Centre Discovery Gardens</option>
                                        <option value="Medcare Medical Centre Marina" readonly>Medcare Medical Centre Marina</option>
                                        <option value="Medcare Eye Centre" readonly>Medcare Eye Centre</option>
                                        <option value="Medcare Medical Centre Sharjah" readonly>Medcare Medical Centre Sharjah</option>
                                        <option value="Medcare IVF" readonly>Medcare IVF</option>
                                        <option value="Medcare Hospital Sharjah" readonly>Medcare Hospital Sharjah</option>
                                        <option value="Medcare Woman &amp; Children Hospital" readonly>Medcare Woman &amp; Children Hospital</option>
                                        <option value="Medcare Medical Centre Motor City" readonly>Medcare Medical Centre Motor City</option>
                                    </select>
								</div>
							</div>




<div class="form-group">
								<label class="col-sm-3 control-label"> DHA License </label>
								<div class="col-sm-9">
<?php
    $is_selected_gp = 'selected';
    $is_selected_specialist = '';
    $is_selected_SpecialistUnderSupervision = '';
    $is_selected_consultant = '';
    $is_selected_physiotherapist = '';
    //$not_selected = '';
    if($row['lice'] == 'GP')
    {
        $is_selected_gp = 'selected';
    }
    if($row['lice']=='Specialist')
    {
    	$is_selected_specialist = 'selected';
    }
    if ($row['lice']=='Specialist Under Supervision') {
    	$is_selected_SpecialistUnderSupervision = 'selected';
    }
    if ($row['lice']=='Consultant') {
    	$is_selected_consultant = 'selected';
    }
    if ($row['lice']=='Physiotherapist') {
    	$is_selected_physiotherapist = 'selected';
    }
   
?>
								<select name="lice"  class="form-control" disabled="" >
									<option value="">Select Value</option>
                                                <option value="GP" <?=$is_selected_gp ?>>GP</option>
                                                <option value="Specialist" <?=$is_selected_specialist?>>Specialist</option>
                                                <option value="Specialist Under Supervision" <?=$is_selected_SpecialistUnderSupervision?>>Specialist Under Supervision</option>
                                                <option value="Consultant"<?=$is_selected_consultant?>>Consultant</option>
                                                <option value="Physiotherapist"<?=$is_selected_physiotherapist?>>Physiotherapist</option>
                                </select>

	</div>
							</div>


							<div class="form-group">
								<label class="col-sm-3 control-label"> Speciality  </label>
								<div class="col-sm-9 multiple-sel">
									<select name="spec[]" multiple="" class="form-control"  style="height:200px;" id="multiselect" disabled="">
                                        <option value="Vascular Surgery">Vascular Surgery</option>
										<option value="Cardiology">Cardiology</option>
										<option value="Dentistry">Dentistry</option>
										<option value="Dermatology">Dermatology</option>
										<option value="Endocrinology">Endocrinology</option>
										<option value="Gastroenterology">Gastroenterology</option>
										<option value="Gynaecology">Gynaecology</option>
										<option value="Internal Medicine & Emergency Care">Internal Medicine & Emergency Care</option>
										<option value="Nephrology">Nephrology</option>
										<option value="Neurosciences">Neurosciences</option>
										<option value="Oncology">Oncology</option>
										<option value="Ophthalmology">Ophthalmology</option>
										<option value="Paediatrics">Paediatrics</option>
										<option value="Psychiatry">Psychiatry</option>
										<option value="General Surgery">General Surgery</option>
										<option value="Urology">Urology</option>
										<option value="Sports Medicine">Sports Medicine</option>
										<option value="Orthopaedics">Orthopaedics</option>
										<option value="Bariatric Surgery">Bariatric Surgery</option>
										<option value="Diet & Nutrition">Diet & Nutrition</option>
										<option value="E.N.T">E.N.T</option>
										<option value="Physiotherapy">Physiotherapy</option>
										<option value="Plastic Surgery">Plastic Surgery</option>
                                    </select>
								</div>
								<!-- <div class="col-sm-9">
									<input type="text" name="spec" required class="form-control" value="">
								</div> -->
							</div>

							
			

	<div class="form-group">
								<label class="col-sm-3 control-label"> Professional introduction </label>
								<div class="col-sm-9">
<textarea name="intro" class="form-control" rows="5" readonly=""><?=isset($row['intro'])?$row['intro']:'' ?></textarea>


								</div>
							</div>

									
						

  </div>
</div>


<?php 
	$data = $row['cred'];
	$arr = explode(",", $data);
 ?>
	
	<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">Education</h3>
  </div>
  <div class="panel-body">
  

					
							<div class="form-group">
								<label class="col-sm-3 control-label"> Credentials	</label>
								<div class="col-sm-9 letplayit">
<div class="row">
<div class="col-xs-3">Certificate</div>
<div class="col-xs-3">University</div>
<div class="col-xs-3">Country</div>
<div class="col-xs-3">Year</div>
</div>

<div class="row therow" style="margin-top:10px;margin-bottom:10px;">
<div class="col-sm-3"><input type="text" name="cred[]" placeholder="Certificate" class="form-control" value=""></div>
<div class="col-sm-3"><input type="text" name="cred[]" placeholder="University"  class="form-control" value=""></div>
<div class="col-sm-3"><input type="text" name="cred[]" placeholder="Country"  class="form-control" value=""></div>
<div class="col-sm-2 col-xs-9"><input name="cred[]" type="text" placeholder="Year"  class="form-control" value=""></div>
<div class="col-sm-1 col-xs-1"><button type="button" class="btn btn-success btn-sm addrow"><i class="glyphicon glyphicon-plus-sign"></i></button></div>
</div>



<div class="rowcontainer"></div>



<div class="newrow" style="display:none;">
<div class="inrow">
<div class="row visible-xs">
<div class="col-xs-3">Certificate</div>
<div class="col-xs-3">University</div>
<div class="col-xs-3">Country</div>
<div class="col-xs-3">Year</div>
</div>

<div class="row thenewrow" style="margin-top:10px;margin-bottom:10px;">
<div class="col-sm-3"><input type="text" name="cred[]" placeholder="Certificate" class="form-control" value=""></div>
<div class="col-sm-3"><input type="text" name="cred[]" placeholder="University"  class="form-control" value=""></div>
<div class="col-sm-3"><input type="text" name="cred[]" placeholder="Country"  class="form-control" value=""></div>
<div class="col-sm-2 col-xs-9"><input name="cred[]" type="text" placeholder="Year"  class="form-control" value=""></div>
<div class="col-sm-1 col-xs-1"><button type="button" class="btn btn-success btn-sm addrow"><i class="glyphicon glyphicon-plus-sign"></i></button></div>
</div>
</div>
</div>


								</div>
							</div>





								</div>
							</div>

	<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Where did you work before?
</h3>
  </div>
  <div class="panel-body">





					
							<div class="form-group">
								<label class="col-sm-3 control-label"> Professional Experience		</label>
								<div class="col-sm-9 letplayit2">
<div class="row">
<div class="col-xs-3">Title</div>
<div class="col-xs-2">Orgnization</div>
<div class="col-xs-2">Country</div>
<div class="col-xs-2 col-xs-6">From</div>
<div class="col-xs-2 col-xs-6">To</div>
</div>



<div class="row therow2" style="margin-top:10px;margin-bottom:10px;">
<div class="col-sm-3"><input type="text" name="exp[]" placeholder="Title" class="form-control" value=""></div>
<div class="col-sm-2"><input type="text" name="exp[]" placeholder="Organization"  class="form-control" value=""></div>
<div class="col-sm-2"><input type="text" name="exp[]" placeholder="Country"  class="form-control" value=""></div>
<div class="col-sm-2 col-xs-5"><input name="exp[]" type="text" placeholder="Year"  class="form-control" value=""></div>
<div class="col-sm-2 col-xs-5"><input name="exp[]" type="text" placeholder="Year"  class="form-control" value=""></div>
<div class="col-sm-1 col-xs-2"><button type="button" class="btn btn-success btn-sm addrow2"><i class="glyphicon glyphicon-plus-sign"></i></button></div>
</div>



<div class="rowcontainer2"></div>



<div class="newrow2" style="display:none;">
<div class="inrow2">
<div class="row visible-xs">
<div class="col-xs-3">Title</div>
<div class="col-xs-2">Orgnization</div>
<div class="col-xs-2">Country</div>
<div class="col-xs-2 col-xs-6">From</div>
<div class="col-xs-2 col-xs-6">To</div>
</div>

<div class="row thenewrow2" style="margin-top:10px;margin-bottom:10px;">
<div class="col-sm-3"><input type="text" name="exp[]" placeholder="Title" class="form-control" value=""></div>
<div class="col-sm-2"><input type="text" name="exp[]" placeholder="Organization"  class="form-control" value=""></div>
<div class="col-sm-2"><input type="text" name="exp[]" placeholder="Country"  class="form-control" value=""></div>
<div class="col-sm-2 col-xs-5"><input name="exp[]" type="text" placeholder="Year"  class="form-control" value=""></div>
<div class="col-sm-2 col-xs-5"><input name="exp[]" type="text" placeholder="Year"  class="form-control" value=""></div>
<div class="col-sm-1 col-xs-2"><button type="button" class="btn btn-success btn-sm addrow2"><i class="glyphicon glyphicon-plus-sign"></i></button></div>
</div>
</div>
</div>


								</div>
							</div>



								
							

							
	


								</div>
							</div>
<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">What are your professional associations?
</h3>
  </div>
  <div class="panel-body">






					
							<div class="form-group">
								<label class="col-sm-3 control-label"> What are your professional associations?

		</label>
								<div class="col-sm-9 letplayit3">




<div class="row therow3" style="margin-top:10px;margin-bottom:10px;">
	
<div class="col-xs-11"><input type="text" name="assoc[]" placeholder="Professional Association	" class="form-control" value=""></div>
<div class="col-xs-1"><button type="button" class="btn btn-success btn-sm addrow3"><i class="glyphicon glyphicon-plus-sign"></i></button></div>
</div>



<div class="rowcontainer3"></div>



<div class="newrow3" style="display:none;">
<div class="inrow3">
<div class="row thenewrow3" style="margin-top:10px;margin-bottom:10px;">
<div class="col-xs-11"><input type="text" name="assoc[]" placeholder="Add another Professional Association	" class="form-control" value=""></div>
<div class="col-xs-1"><button type="button" class="btn btn-success btn-sm addrow3"><i class="glyphicon glyphicon-plus-sign"></i></button></div>
</div>
</div>
</div>


								</div>
							</div>



								</div>
							</div>

	<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">What are your areas of clinical expertise?</h3>
  </div>
  <div class="panel-body">
							<div class="form-group">
								<label class="col-sm-3 control-label"> Clinical Expertise				</label>
								<div class="col-sm-9 letplayit4">




<div class="row therow4" style="margin-top:10px;margin-bottom:10px;">
<div class="col-xs-11"><input type="text" name="exper[]" placeholder="Clinical Expertise" class="form-control" value=""></div>
<div class="col-xs-1"><button type="button" class="btn btn-success btn-sm addrow4"><i class="glyphicon glyphicon-plus-sign"></i></button></div>
</div>



<div class="rowcontainer4"></div>



<div class="newrow4" style="display:none;">
<div class="inrow4">
<div class="row thenewrow4" style="margin-top:10px;margin-bottom:10px;">
<div class="col-xs-11"><input type="text" name="exper[]" placeholder="Add another Clinical Expertise" class="form-control" value=""></div>
<div class="col-xs-1"><button type="button" class="btn btn-success btn-sm addrow4"><i class="glyphicon glyphicon-plus-sign"></i></button></div>
</div>
</div>
</div>


								</div>
							</div>




					

  </div>
</div>




















<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title"> Published articles and researches</h3>
  </div>
  <div class="panel-body">
							<div class="form-group">
								<label class="col-sm-3 control-label"> Published articles and researches			</label>
								<div class="col-sm-9 letplayit5">




<div class="row therow5" style="margin-top:10px;margin-bottom:10px;">
<div class="col-xs-11"><input type="text" name="articles[]" placeholder="Published articles and researches" class="form-control" value=""></div>
<div class="col-xs-1"><button type="button" class="btn btn-success btn-sm addrow5"><i class="glyphicon glyphicon-plus-sign"></i></button></div>
</div>



<div class="rowcontainer5"></div>



<div class="newrow5" style="display:none;">
<div class="inrow5">
<div class="row thenewrow5" style="margin-top:10px;margin-bottom:10px;">
<div class="col-xs-11"><input type="text" name="articles[]" placeholder="Add another articles" class="form-control" value=""></div>
<div class="col-xs-1"><button type="button" class="btn btn-success btn-sm addrow5"><i class="glyphicon glyphicon-plus-sign"></i></button></div>
</div>
</div>
</div>


								</div>
							</div>




					

  </div>
</div>























<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Awards and accolades</h3>
  </div>
  <div class="panel-body">
							<div class="form-group">
								<label class="col-sm-3 control-label">Awards and accolades			</label>
								<div class="col-sm-9 letplayit6">



<div class="row therow6" style="margin-top:10px;margin-bottom:10px;">
<div class="col-xs-11"><input type="text" name="awards[]" placeholder="Awards and accolades" class="form-control" value=""></div>
<div class="col-xs-1"><button type="button" class="btn btn-success btn-sm addrow6"><i class="glyphicon glyphicon-plus-sign"></i></button></div>
</div>



<div class="rowcontainer6"></div>



<div class="newrow6" style="display:none;">
<div class="inrow6">
<div class="row thenewrow6" style="margin-top:10px;margin-bottom:10px;">
<div class="col-xs-11"><input type="text" name="awards[]" placeholder="Add another Awards" class="form-control" value=""></div>
<div class="col-xs-1"><button type="button" class="btn btn-success btn-sm addrow6"><i class="glyphicon glyphicon-plus-sign"></i></button></div>
</div>
</div>
</div>


								</div>
							</div>




					

  </div>
</div>








<input type="submit" name="edit" value="Edit" class="btn purber" onclick="$(this).val('Editing');" />
<input type="submit" name="submit" value="Submit" class="btn purber" onclick="$(this).val('Sending');" />


											
	

	</form>


				
			</div>
    </div>
		
<script>

$( document ).ready(function() {


// $('#multiselect').multiselect();







$(".letplayit,.letplayit3,.letplayit4,.letplayit5,.letplayit6,.letplayit2").on("click", '.removerow,.removerow3,.removerow2,.removerow4,.removerow5,.removerow6', function(){

if(confirm('Are you sure to remove this line')){
$(this).parent().parent().parent().remove();
}

});






});



</script>	
		
		
<?php
include('footer.php');
} else {
    echo '<div class="container"><div class="alert alert-danger">No Data Found </div> </div>';
    exit;
}
?>